# Autopilot

`Autopilot` is the automatic deployment tool for Spock. With autopilot, we can deploy Spock easily and flexibly.

There are several scenarios to use autopilot:

* Deploy Spock on cloud or in air-gapped environment
* Deploy Spock in multi-nodes or standalone mode
* Deploy Spock continuously on specific env when triggerred

## Design Principles

* Based on `Kubernetes`, `KubeSphere` is the system managing our containerized applications and resources. It natively integates many third-party applications, like `Istio` and `OpenEBS`, which are necessary in our scenarios. And KubeSphere can be installed and run on any infrastructure
* `Kubekey`, as our choice of installing Kubernetes and KubeSphere, supports KubeSphere's customized installation natively
* `Helm`, as well as `Kustomize`, has few invasion in the Kubernetes manifests and provides YAML-formatted congifurations
* Basically, we can consider each command line as an ansible tasks; Each ansible playbook consists of many tasks. The deployment procedure is completed by running bunch of ansible playbooks
* We choose `Ansible` to automate every step in the whole deploying procedure. In the comparison with other automation tools, like `Chef`, `Puppet` and `Saltstack`:
  * Ansible is much easiler to setup on any environment
  * Tasks can be easily managed and scaled up by using its playbook and modules 
  * Using `YAML` as configuration language make the learning curve gentle
* The root directory of autopilot is our workspace, we must execute all ansible command under this specific path because the customized `ansibel.cfg` is here
* `base_config.yaml` is the only and the most important configuration file for all of our ansible operations. Basically, it is just the ansible inventory including a lot of variables that can penetrate to playbooks or k8s manifests through Jinjia2's syntax. It controls what pods run on specific nodes, how we deploy the application and so on
* In short, the procedures of deploying Spock on a bare metal machine can be:
  * Bootstrap the operating system
  * Setup docker registry (if in air-gapped mode)
  * Install Kubernetes and KuberSphere
  * Prepare the manifests of each application
  * Deploy them into Kubernetes
  * Initialize the database and pachyderm data
* Using local persistent volumes(from specific one filesystem) for dynamic provisioning is our only storage design currently. Although this might be a perfect design for distributed application prefers strong data consisitency (if we have SSD), it is just temporary and the space is limited

## Requirements and Recommendations

### Specification Recommendations

* Resource recommendations for each node in multi-nodes(4 nodes suggested) mode：
  * 8 vCPUs
  * 16 GB RAM
  * 300 GB Storage
  * 1 Nvidia T4 GPU (Further compatibility investigation is needed)
* Resource recommendations for the node of standalone mode：
  * 8 vCPUs
  * 32 GB RAM
  * 1 TB Storage
  * 1 Nvidia T4 GPU (Further compatibility investigation is needed)

### OS Requirements

* Ubuntu 18.04 Bionic only (for now)
* `SSH` can access to all nodes
* Time synchronization for all nodes
* `sudo`/`curl`/`openssl` should be used in all nodes
* `sudo swapoff -a` Turning off the swap is recommended

### Dependency Recommendations

Clean OS would be the best, we can install all dependencies by using autopilot. But if we have to install components with specific version separately or reuse the existing dependency, we should consider those with recommended versions listed below:

| Services for Spock | Version | Deployment Tools | Version                      |
| ------------------ | ------- | ---------------- | ---------------------------- |
| Docker             | 20.10.2 | Ansible          | 2.5.1                        |
| Kubernetes         | v1.20.4 | kubectl          | v1.20.4                      |
| KubeSphere         | v3.1.0  | kubekey          | 1.1.0                        |
| MinIO              | 6.0.5   | mc               | RELEASE.2021-06-13T17-48-22Z |
| Pachyderm          | 1.13.3  | pachctl          | 1.13.3                       |
| KubeFlow Pipeline  | 1.6.0   | helm             | v3.2.1                       |
| Seldon-core        | 1.9.0   |                  |                              |

### Networking and DNS Requirements

* Make sure the DNS address in `/etc/resolv.conf` is available. Otherwise, it may cause some issues of DNS in cluster.
* If the network configuration uses Firewall or Security Group，we must ensure infrastructure components can communicate with each other through specific ports. It's recommended that we turn off the firewall or ensure the access below: 

  | Service        | Protocol       | Action | Start Port | End Port | Notes                                   |
  | -------------- | -------------- | ------ | ---------- | -------- | --------------------------------------- |
  | ssh            | TCP            | allow  | 22         |          |                                         |
  | etcd           | TCP            | allow  | 2379       | 2380     |                                         |
  | apiserver      | TCP            | allow  | 6443       |          |                                         |
  | calico         | TCP            | allow  | 9099       | 9100     |                                         |
  | bgp            | TCP            | allow  | 179        |          |                                         |
  | nodeport       | TCP            | allow  | 30000      | 32767    | default range                           |
  | master         | TCP            | allow  | 10250      | 10258    |                                         |
  | dns            | TCP/UDP        | allow  | 53         |          |                                         |
  | local-registry | TCP            | allow  | 5000       |          | For the offline environment             |
  | local-apt      | TCP            | allow  | 5080       |          | For the offline environment             |
  | rpcbind        | TCP            | allow  | 111        |          | Required if NFS is used                 |
  | ipip           | IPENCAP / IPIP | allow  |            |          | Calico needs to allow the ipip protocol |
  | metrics-server | TCP            | allow  | 8443       |          |                                         |
  | Spock Service  | TCP            | allow  | 32080      |          | NodePort Service                        |
  | Spock UI       | TCP            | allow  | 32090      |          | NodePort Service                        |

## Layout

The airgapped installation pack should have the layout below:
```
├── README.md
├── ansible.cfg                     // configuration for ansible
├── ansible.tar.gz                  // ansible installation package
├── base_config.yml                 // ansible inventory and variables
├── base-models/                    // base models need uploading the pachyderm in advance 
│   ├── dashed_font.tar
│   └── regular_font.tar 
├── docs                            // some guide and investigation docs
├── images                          // docker images need to be loaded into private docker registry (needed by air-gapped mode)
│   ├── ...                         // all the docker images are store here (needed by air-gapped mode)
│   └── registry.tar.gz
├── images-list.txt
├── kk                              // kubekey binary file
├── kubekey                         // other binary files like kubectl, kubeadm and so on
├── logs                            // ansible runtime log and kubekey log
├── manifests                       // Kubernetes manifests
│   ├── kfp-kustomize/
│   ├── minio/
│   ├── seldon-core-operator/
│   └── spock/
├── offline-installation-tool.sh    // script of saving and loading  
├── packages.tar.gz                 // packages need to be installed or uploaded  
└── playbooks                       // ansible playbooks and the included roles
    ├── bootstrap.yaml
    ├── deploy_kfp.yaml
    ├── deploy_pachyderm.yaml
    ├── deploy_registry.yaml
    ├── deploy_seldoncore.yaml
    ├── install_kubesphere.yaml
    └── roles
        ├── docker
        ├── kfp
        ├── kubersphere
        ├── minio
        ├── nvidia-container-runtime
        ├── nvidia-device-plugin
        ├── nvidia-pachyderm-plugin
        └── seldon
```

## Usage

For now, autopilot can be used to install or deploy the whole Spock infrastructure applications.

### Package the installation pack

**If we are going to start the air-gapped deployment, the following steps will help build a complete installation pack, by which we don't need Internet connection during deployment on those environments**

***1. Pull and save docker images***

All the images we need have been pushed to `registry.senetime.com/industry`, we will pull those images and save them into our package. So make sure the *company VPN* is connected before start.
```
$ cd autopilot
$ ./offline-installation-tool.sh -s -b -l imagelist.txt  -d ./images -v v1.20.4
```

***2. Download packages from OSS***

Besides docker images' tarballs, we still need many tools(binary files or debian packages) and base models in our package. They are all managed in the OpenCloud Ceph OSS, `s3://spock/` bucket. For downloading files, we need to install `s3cmd` first by following this [document](https://confluence.sensetime.com/pages/viewpage.action?pageId=28650342). Make sure the *company VPN* is connected before start.
```
$ cd autopilot
$ s3cmd get s3://spock/kk
$ s3cmd get s3://spock/ansible.tar.gz
$ s3cmd get s3://spock/packages.tar.gz
$ s3cmd get -r  s3://spock/base-models
```

### Before Deployment

***1. Confirm the system user***

We need to create a dedicated user to run our deployment, especially to run docker. And we need this user to have `sudo` permission in order to install ansible and bootstrap machine.
So for every single machine, please run
```
$ sudo -s
# useradd sensetime
# passwd sensetime            // Make sure the passwords are identical
# echo -e "sensetime ALL=(ALL) NOPASSWD:  ALL" > /etc/sudoers.d/sensetime
```
We need to pick one machine as the **Center Control Machine**, on which we can ssh to other machines and execute commands via ansible.

*If we need ssh password to ssh to any other nodes, then we should add `-k` flag behind every ansible or ansible-playbook command line.*

*If we need ssh private key to ssh to any other nodes, then we should add `--private-key=/path/to/your/private/key` flag behind every ansible or ansible-playbook command line.*

*If we need sudo permission for some ansible tasks, then we should add `-K` flag behind every ansible or ansible-playbook command line.*

***2. Confirm mounted disk***

Currently, we only support dynamic provisioning from single directly. So using `lvm` to group the same kind of volumes is suggested, then create logic volume from the volume group.
So for every single machine, please run
```
$ sudo -s
# mkdir /data
# mkfs.ext4 /dev/mapper/VolGroup00-LVdata
# echo /dev/mapper/VolGroup00-LVdata /data ext4 defaults 0 0 >> /etc/fstab
# mount /dev/mapper/VolGroup00-LVdata /data
```

***3. Install Ansible***

We need to pick one machine as the **Center Control Machine**, which can ssh to other machines and execute commands via ansible.
**Upload the installation package to the center control machine and untar this package, then run**
```
$ cd autopilot
$ tar zxvf ansible.tar.gz
$ cd ansible
# dpkg -i --force *.deb
```
or if we have Internet connection,
```
# apt install ansible
```

***4. Congifure the environment***

We need to edit the `base_config.yaml` to decide how to deploy and what the cluster will be like.
```
all:
  hosts:
    10.53.25.41:
    10.53.25.42:
    10.53.24.209:
  children:                                 // We can create the group of nodes as a specific ansible inventory
    gpu:
      hosts:                                // List the nodes with gpu here, drivers and device-plugin will be installed on those nodes only
        10.53.25.41:
        10.53.25.42:
        10.53.24.209:
    registry:
      hosts:
        10.53.25.41:                        // Docker registry will be deployed on this node

  vars:                                     // variables definition starts here
    airgapped_mode: true  
    k8s_hosts:                              // Basically, every node needs adding into the k8s cluster should show up here 
      - ip: 10.53.25.41
        hostname: node1 
        roles:
        - master
        - etcd
        - worker
      - ip: 10.53.25.42
        hostname: node2
        roles:
        - etcd
        - worker
      - ip: 10.53.24.209
        hostname: node3
        roles:
        - etcd
        - worker
    data_dir: "/data"
    runtime_user: "sensetime"
    minio_accesskey: "minioha"
    minio_secretkey: "miniohasec"
    minio_drives_per_node: 2
    minio_replicas: 6
    minio_pv_size: "100Gi"
    minio_nodeport: 32000
```
### Deployment

***1. Bootstrap***

Now we are ready to deploy our stack. First of all, we should bootstrap each machine and prepare the environment for the actual installations and configurations.
```
$ cd autopilot
$ ansible-playbook playbooks/bootstrap.yaml
```

***2. Deploy Docker Registry (optional)***

*For air-gapped deployment only*
```
$ cd autopilot
$ ansible-playbook playbooks/deploy_registry.yaml
```

***3. One step to deploy everything***

```
$ cd autopilot
$ ansible-playbook playbooks/deploy_all.yaml
```

### Remove

We can use `kubekey` to uninstall the whole KubeSphere and Kubernetes cluster.
```
$ ./kk delete cluster -f cluster_config.yaml
$ docker stop regisry && docker rm registry // If it's air-gapped deployment
$ docker images prune -a  (type 'yes')    // Remove local images if necessary
```
## Documents

* [Project Gateway](/docs/project-gateway)