# Project Gateway

A [gateway](https://kubesphere.io/docs/project-administration/project-gateway) in a KubeSphere project is an NGINX Ingress controller. KubeSphere has a built‑in configuration for HTTP load balancing, called Routes. A [Route](https://kubesphere.io/docs/project-user-guide/application-workloads/routes/) defines rules for external connections to Services within a cluster. A Route on KubeSphere is the same as an Ingress on Kubernetes.

Users who need to provide external access to their Services create a Route resource that defines rules, including the URI path, backing service name, and other information.

## How to set the gateway up

* Login KubeSphere as the role of `project-admin` or higher authority
* Workbench -> Workspaces -> *Select the workspace* -> Projects -> *Select the project* -> Project Settings -> Advanced Settings
* Press *Set Gateway*
* Choose the access mode of gateway
* Press *Save*

Setting up the gateway is the prerequisite of using route(ingress) in KubeSphere.

Follow these instructions, a deployment named `kubesphere-router-<project-name>` and a NodePort service named `kubesphere-router-<project-name>` will be created in the project(namespace) `kubesphere-control-system` which can only be seen by admin.

The deployment is the nginx ingress controller, and the service exposes the 80 and 443 ports of ingress controller through random nodeports. **Remember**, the default range of our nodeport is from 30000 to 32766, we can find `/etc/kubernetes/manifests/kube-apiserver.yaml` on our central control node, change `--service-node-port-range=30000-32767` to `--service-node-port-range=1-65535`. And KubeSphere will load it automatically after a few seconds.


## How to create routes(ingress)

* Login KubeSphere as the role of `project-regular` or higher authority
* Workbench -> Workspaces -> *Select the workspace* -> Projects -> *Select the project* -> Application Workloads -> Routes
* Press *Create*

If we need to resolve some domain to our service, we should purchase the domain and resolve it to our node in advance.

## How to add nginx configurations

We can add [Kubernetes annotations](https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/annotations/) to specific Ingress objects to customize their behavior.

* Login KubeSphere as the role of `project-regular` or higher authority
* Workbench -> Workspaces -> *Select the workspace* -> Projects -> *Select the project* -> Application Workloads -> Routes -> *Find the specific Route* -> More -> Edit Annotations
* Find the annotation matches your configuration and add it; for example, `client_max_body_size` corresponds to `nginx.ingress.kubernetes.io/proxy-body-size:`.
* Press *OK*

After we create the Routes or the Annoations of Routes, all the infomation will show up if we `kubectl get ingress <route-name> -n<your-project-name>`. All the rules and annotations will become configurations of the `/etc/nginx/nginx.conf` inside the nginx ingress conntroller container.
